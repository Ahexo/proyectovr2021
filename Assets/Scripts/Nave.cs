// Alejandro Axel Rodr�guez S�nchez
// ahexo@ciencias.unam.mx

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Nave : MonoBehaviour {

    public int velocidad;
    bool aBordo;

    // Start is called before the first frame update
    void Start() {
        
    }

    // Update is called once per frame
    void Update() {
        if (aBordo) {
            transform.position = transform.position + Camera.main.transform.forward * velocidad * Time.deltaTime;
        }
    }
}

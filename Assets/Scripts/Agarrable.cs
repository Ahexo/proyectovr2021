using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Agarrable : MonoBehaviour {

    public GameObject sujetoA;
    public Material materialActivo;
    public Material materialInactivo;
    public Material materialSeleccionado;
    public Renderer meshRenderer;

    // Update is called once per frame
    void Start() {
        meshRenderer = GetComponent<Renderer>();
    }

    public void sujetarA (GameObject pivote) {
        GetComponent<MeshCollider>().isTrigger = true;
        GetComponent<Rigidbody>().useGravity = false;

        this.transform.parent = pivote.transform;
        this.transform.position = pivote.transform.position;

        meshRenderer.material = materialSeleccionado;

        sujetoA = pivote;
    }

    public void soltar () {
        sujetoA = null;

        meshRenderer.material = materialInactivo;
        this.transform.parent = null;

        GetComponent<MeshCollider>().isTrigger = false;
        GetComponent<Rigidbody>().useGravity = true;
    }


    public void OnPointerEnter() {
        SetMaterial(true);
    }

    public void OnPointerExit() {
        SetMaterial(false);
    }

    private void SetMaterial(bool gazedAt) {
        if (materialInactivo != null && materialActivo != null && sujetoA == null) {
            meshRenderer.material = gazedAt ? materialActivo : materialInactivo;
            }
        }
}
# Proyecto VR 2022-1

**Facultad de Ciencias UNAM**
**Realidad Virtual, Grupo 7117, semestre 2022-1**
**Profesora:** María Concepción Ana Luisa Solís González Cosío
**Ayudante:** Daniel Ruelas Milanés

**Alumno:** Alejandro Axel Rodríguez Sánchez


## Recursos 

Este proyecto hace uso de los siguientes recursos de uso abierto: 

+ [Google Cardboard XR Plugin for Unity](https://github.com/googlevr/cardboard-xr-plugin), de Google LLC.

+ [Handpainted Grass & Ground Textures](https://assetstore.unity.com/packages/2d/textures-materials/handpainted-grass-ground-textures-187634), de Chromisu.

+ [Space Kit](https://kenney.nl/assets/space-kit), de Kenney.nl. 

+ [Modular First Person Controller](https://assetstore.unity.com/packages/3d/characters/modular-first-person-controller-189884), de Jess Case.